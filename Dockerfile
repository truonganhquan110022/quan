FROM nginx:latest
COPY ./nginx.conf	    /etc/nginx/nginx.conf
COPY ./testlive /usr/share/nginx/html
EXPOSE 80
WORKDIR /testlive

